# Re-mapping the Touch Bar MBP's keyboard to more closely match the Apple M0116 keyboard layout (using Karabiner-Elements)

I've since started using the Apple M0116 keyboard (with orange Alps switches), and I've gotten so used to the layout, I've decided to re-map the internal keyboard to match.

These configs get dropped into `~/.config/karabiner/assets/complex_modifications/` and enabled via the Karabiner-Elemnts UI

Changing the backtick/tilde to escape, and Caps Lock to left Control, is done under Simple Modifications:

img here

Other changes are done under Complex Modifications. Here are the ones I have so far:


